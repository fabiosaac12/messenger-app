import React from 'react';
import { App } from 'components/App';
import { LanguageProvider } from 'components/Language';
import { ThemeProvider } from 'components/Theme';
import { ModalProvider } from 'components/Modal';
import { LoaderProvider } from 'components/Loader';
import { ContextLogger } from 'components/ContextLogger';
import { config } from 'providers/config';
import { translations } from './translations';
import appContext from './context';
import { AuthProvider } from 'components/Auth';
import { SocketProvider } from 'components/Socket';
import { SideDrawerProvider } from 'components/SideDrawer';
import { UsersProvider } from 'components/Users';
import { MessagesProvider } from 'components/Messages';
import { SelectedChatProvider } from 'components/SelectedChat';
import { RoomsProvider } from 'components/Rooms';
import { NotificationsProvider } from 'components/Notifications';

const contextConfig = { objectDiffs: true, arrayDiffs: false };

export function Root() {
  return (
    <>
      <ThemeProvider defaultTheme={config.siteConfig.defaultTheme}>
        <LanguageProvider
          locale={config.siteConfig.languageCode}
          translations={translations}
        >
          <LoaderProvider>
            <ModalProvider>
              <SideDrawerProvider>
                <SocketProvider url="//localhost:4000">
                  <AuthProvider>
                    <UsersProvider>
                      <RoomsProvider>
                        <SelectedChatProvider>
                          <MessagesProvider>
                            <NotificationsProvider>
                              <App />
                              <ContextLogger
                                contexts={appContext}
                                config={contextConfig}
                              />
                            </NotificationsProvider>
                          </MessagesProvider>
                        </SelectedChatProvider>
                      </RoomsProvider>
                    </UsersProvider>
                  </AuthProvider>
                </SocketProvider>
              </SideDrawerProvider>
            </ModalProvider>
          </LoaderProvider>
        </LanguageProvider>
      </ThemeProvider>
    </>
  );
}
