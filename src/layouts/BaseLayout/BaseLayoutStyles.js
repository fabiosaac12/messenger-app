import { Images } from 'assets';

const { makeStyles } = require('@material-ui/core');

export const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    backgroundImage:
      theme.palette.type === 'dark'
        ? `url(${Images.robotWhite})`
        : `url(${Images.robotBlack})`,
    backgroundSize: '100%',
    backgroundRepeat: 'no-repeat'
  },
  container: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column'
  }
}));
