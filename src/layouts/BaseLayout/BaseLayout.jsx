import React from 'react';
import PropTypes from 'prop-types';
import { Header } from 'components/Header';
import { Loader } from 'components/Loader';
import { Modal } from 'components/Modal';
import { SideDrawer } from 'components/SideDrawer';
import { useStyles } from './BaseLayoutStyles';

function BaseLayout({ children }) {
  const classes = useStyles();

  return (
    <>
      <SideDrawer />
      <Loader />
      <Modal />
      <div className={classes.wrapper}>
        <Header />
        <div className={classes.container}>{children}</div>
      </div>
    </>
  );
}

BaseLayout.propTypes = {
  children: PropTypes.node.isRequired
};

export { BaseLayout };
