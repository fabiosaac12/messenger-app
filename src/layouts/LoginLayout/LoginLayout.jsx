import React from 'react';
import PropTypes from 'prop-types';
import { Loader } from 'components/Loader';
import { useStyles } from './LoginLayoutStyles';
import { Images } from 'assets';
import { useTheme } from '@material-ui/core';

function LoginLayout({ children }) {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <>
      <Loader />
      <div className={classes.container}>
        <img
          className={classes.logo}
          src={
            theme.palette.type === 'light'
              ? Images.logoDarkNoRounded
              : Images.logoLightNoRounded
          }
          alt=""
        />
        {children}
      </div>
    </>
  );
}

LoginLayout.propTypes = {
  children: PropTypes.node.isRequired
};

export { LoginLayout };
