import { Images } from 'assets';

const { makeStyles } = require('@material-ui/core');

export const useStyles = makeStyles((theme) => ({
  container: {
    minHeight: '100vh',
    backgroundImage:
      theme.palette.type === 'dark'
        ? `url(${Images.backgroundWhite})`
        : `url(${Images.backgroundBlack})`,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    backgroundSize: '120px'
  },
  logo: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '170px'
  }
}));
