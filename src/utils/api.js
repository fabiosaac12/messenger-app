import axios from 'axios';
import { config } from 'providers/config';

export const createInstance = ({ ...props }) => {
  const instance = axios.create({
    baseURL: config.endpoints.mainBackendUrl,
    timeout: 60000,
    headers: {
      'Content-Type': 'application/json'
    },
    ...props
  });

  instance.interceptors.request.use(
    async (config) => {
      try {
        const token = localStorage.getItem('token');

        return {
          ...config,
          headers: {
            ...config.headers,
            Authorization: `Bearer ${token}`
          }
        };
      } catch (e) {
        console.error(e);
      }

      return config;
    },
    (error) => Promise.reject(error)
  );

  instance.interceptors.response.use(
    (response) => response,
    async (error) => await Promise.reject(error)
  );

  return instance;
};
