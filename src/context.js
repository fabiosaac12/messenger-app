import { LanguageContext } from 'components/Language/LanguageContext';
import { ThemeContext } from 'components/Theme/ThemeContext';
import { ModalContext } from 'components/Modal/ModalContext';
import { LoaderContext } from 'components/Loader/LoaderContext';

const appContext = {
  language: LanguageContext,
  theme: ThemeContext,
  modal: ModalContext,
  loader: LoaderContext
};

export default appContext;
