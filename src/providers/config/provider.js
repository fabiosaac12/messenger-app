export const config = {
  appId: (process.env.REACT_APP_NAME || '').replace(/@octopy\/react-spa-/, ''),
  siteConfig: {
    languageCode: (process.env.REACT_APP_LANGUAGE_CODE || '').toLowerCase(),
    defaultTheme: process.env.REACT_APP_DEFAULT_THEME,
    allowBrowserTheme:
      process.env.REACT_APP_THEME_ENABLE_BROWSER_SUPPORT === 'true'
  },
  endpoints: {
    mainBackendUrl: process.env.REACT_APP_MAIN_BACKEND_URL
  }
};
