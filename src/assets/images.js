import jsIcon from './svg/javascript-icon.svg';
import reactIcon from './svg/react-js-icon.svg';
import scssIcon from './svg/sass-icon.svg';
import svgIcon from './svg/sun-icon.svg';
import folderIcon from './svg/file-manager.svg';
import folderOpenIcon from './svg/folder-open-icon.svg';
import imageIcon from './svg/image-icon.svg';
import bracketsIcon from './svg/brackets-icon.svg';
import backgroundBlack from './svg/background-black.svg';
import backgroundWhite from './svg/background-white.svg';
import robotBlack from './svg/logo-black.svg';
import robotWhite from './svg/logo-white.svg';
import logo from './png/logo.png';
import logoWhite from './png/logo-white.png';
import logoBlack from './png/logo-black.png';
import logoDarkNoRounded from './png/logo-dark-no-rounded.png';
import logoLightNoRounded from './png/logo-light-no-rounded.png';

const Images = {
  jsIcon,
  reactIcon,
  scssIcon,
  svgIcon,
  folderIcon,
  folderOpenIcon,
  imageIcon,
  bracketsIcon,
  backgroundBlack,
  backgroundWhite,
  robotBlack,
  robotWhite,
  logo,
  logoWhite,
  logoBlack,
  logoDarkNoRounded,
  logoLightNoRounded
};

export { Images };
