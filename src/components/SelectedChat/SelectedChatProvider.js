import { useEffect, useState } from 'react';
import { SelectedChatContext } from './SelectedChatContext';
import { useAuth } from 'components/Auth';

export const SelectedChatProvider = ({ children }) => {
  const [selectedChat, setSelectedChat] = useState(null);
  const { user } = useAuth();

  useEffect(() => {
    setSelectedChat(null);
  }, [user]);

  const contextValue = {
    selectedChat,
    setSelectedChat: (type, chat) => setSelectedChat({ type, chat })
  };

  return (
    <SelectedChatContext.Provider value={contextValue}>
      {children}
    </SelectedChatContext.Provider>
  );
};
