import { useContext } from 'react';
import { SelectedChatContext } from '../SelectedChatContext';

export const useSelectedChat = () => useContext(SelectedChatContext);
