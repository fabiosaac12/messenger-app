import { Paper, Typography } from '@material-ui/core';
import { useRef, useEffect } from 'react';
import { useMessages } from 'components/Messages';
import { useSelectedChat } from 'components/SelectedChat';
import { useStyles } from './MessagesStyles';
import { useAuth } from 'components/Auth';
import { useSocket } from 'components/Socket';

export const Messages = () => {
  const {
    selectedChat: { type, chat }
  } = useSelectedChat();
  const { getMessages } = useMessages();
  const { user } = useAuth();
  const socket = useSocket();
  const classes = useStyles();
  const messages = getMessages(chat);

  const bottomRef = useRef(null);

  const moveToBottom = (message, behavior = 'smooth') => {
    if (message && message.from !== user && chat !== message.from) return;

    bottomRef.current.scrollIntoView({ behavior });
  };

  useEffect(() => moveToBottom(null, 'auto'), [chat]);

  useEffect(() => {
    socket && socket.on('new_message', moveToBottom);

    return () => socket.off('new_message', moveToBottom);
  }, [socket]);

  return (
    <Paper className={classes.paper}>
      {messages.length > 0 ? (
        messages.map((message, i) => {
          const own = user === message.from;
          const room = type === 'room';

          const messageHeader = !own && room && (
            <Typography className={classes.messageHeader} variant="subtitle2">
              {message.from}
            </Typography>
          );

          return (
            <div
              key={`message${i}`}
              className={own ? classes.selfMessageContainer : ''}
            >
              <Paper
                className={`${own && classes.selfMessage}
                  ${classes.message}`}
              >
                {messageHeader}
                <Typography className={classes.text} variant="body2">
                  {message.text}
                </Typography>
              </Paper>
            </div>
          );
        })
      ) : (
        <Typography variant="subtitle2" className={classes.noMessages}>
          No messages here...
        </Typography>
      )}
      <div className={classes.bottomDiv} ref={bottomRef} />
    </Paper>
  );
};
