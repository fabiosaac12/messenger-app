import { useContext } from 'react';
import { MessagesContext } from '../MessagesContext';

export const useMessages = () => useContext(MessagesContext);
