import { makeStyles } from '@material-ui/core';
import { Images } from 'assets';

export const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundImage:
      theme.palette.type === 'dark'
        ? `url(${Images.backgroundWhite})`
        : `url(${Images.backgroundBlack})`,
    backgroundSize: '100px',
    borderRadius: '0px',
    height: '65vh',
    overflowY: 'auto',
    '&::-webkit-scrollbar': {
      display: 'none'
    },
    [theme.breakpoints.down('sm')]: {
      height: '82%'
    },
    padding: '0 9px 0 9px'
  },
  selfMessageContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  message: {
    maxWidth: '80%',
    boxSizing: 'border-box',
    backgroundColor: theme.palette.primary.main,
    padding: theme.spacing(1.5),
    margin: theme.spacing(0.3),
    marginLeft: theme.spacing(0.6),
    display: 'inline-block',
    borderRadius: '19px',
    borderBottomLeftRadius: '0'
  },
  selfMessage: {
    backgroundColor: theme.palette.primary.light,
    marginRight: theme.spacing(0.6),
    borderRadius: '19px',
    borderBottomRightRadius: '0'
  },
  messageHeader: {
    color:
      theme.palette.type === 'dark'
        ? theme.palette.grey[500]
        : theme.palette.grey[300]
  },
  text: {
    maxWidth: '100%',
    overflowWrap: 'break-word',
    color: theme.palette.common.white
  },
  bottomDiv: {
    height: theme.spacing(1)
  },
  noMessages: {
    textAlign: 'center',
    marginTop: '45%'
  }
}));
