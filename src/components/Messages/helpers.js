import { getItem, setItem } from 'utils/persistentStorage';

const setNotification = (user, chat, type = 'message') => {
  const users = getItem('users');
  const userData = {
    username: user,
    rooms: [],
    friends: [],
    chats: {},
    notifications: {},
    ...(users[user] || {})
  };
  const notifications = userData.notifications;

  const updatedUsers = {
    ...users,
    [user]: {
      ...userData,
      notifications: {
        ...notifications,
        [chat]: { type }
      }
    }
  };

  setItem('users', updatedUsers);
};

const setRoomNotification = (room, type = 'message') => {
  const users = getItem('users');
  const roomUsers = Object.values(users).filter((user) =>
    user.rooms.includes(room)
  );

  for (let user of roomUsers) {
    setNotification(user.username, room, type);
  }
};

const getUserChats = (user) => {
  const users = getItem('users');
  const userData = {
    username: user,
    rooms: [],
    friends: [],
    chats: {},
    notifications: {},
    ...(users[user] || {})
  };

  return userData.chats || {};
};

const getChatWithToKey = (user, to) => getUserChats(user)[to];

const createNewPrivateChatInLocalStorage = (from, to) => {
  const chats = getItem('chats') || {};
  const newChatKey = [from, to].sort().join('_');

  const updatedChats = {
    ...chats,
    [newChatKey]: []
  };

  setItem('chats', updatedChats);

  const users = getItem('users');
  const userData = {
    username: from,
    rooms: [],
    friends: [],
    chats: {},
    notifications: {},
    ...(users[from] || {})
  };
  const toUserData = {
    username: to,
    rooms: [],
    friends: [],
    chats: {},
    notifications: {},
    ...(users[to] || {})
  };
  const userChats = userData.chats;
  const toUserChats = toUserData.chats;
  const userFriends = userData.friends || [];
  const toUserFriends = toUserData.friends || [];

  const updatedUsers = {
    ...users,
    [from]: {
      ...userData,
      chats: {
        ...userChats,
        [to]: newChatKey
      },
      friends: [...userFriends, to]
    },
    [to]: {
      ...toUserData,
      chats: {
        ...toUserChats,
        [from]: newChatKey
      },
      friends: [...toUserFriends, from]
    }
  };

  setItem('users', updatedUsers);

  return newChatKey;
};

export const savePrivateMessageInLocalStorage = (message) => {
  console.log(message);

  const { from, to } = message;

  let chatWithToKey = getChatWithToKey(from, to);

  if (!chatWithToKey) {
    chatWithToKey = createNewPrivateChatInLocalStorage(from, to);
  }

  const chats = getItem('chats');
  const chatWithTo = chats[chatWithToKey];

  const updatedChats = {
    ...chats,
    [chatWithToKey]: [...chatWithTo, message]
  };

  setItem('chats', updatedChats);

  setNotification(to, from);
};

export const saveRoomMessageInLocalStorage = (message) => {
  const { to } = message;

  const chats = getItem('chats') || {};
  const roomChat = chats[to] || [];
  const updatedChats = {
    ...chats,
    [to]: [...roomChat, message]
  };

  setItem('chats', updatedChats);

  setRoomNotification(to);
};

export const getPrivateMessages = (user) => {
  const chats = getItem('chats') || {};
  const userChats = getUserChats(user) || [];

  const privateMessages = {};

  for (let to in userChats) {
    const chatWithToKey = userChats[to];
    const chatWithTo = chats[chatWithToKey];

    privateMessages[to] = chatWithTo;
  }

  return privateMessages;
};

export const getRoomMessages = (room) => {
  const chats = getItem('chats');
  const roomMessages = chats ? chats[room] : [];

  return roomMessages;
};
