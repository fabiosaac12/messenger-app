import { useSocket } from 'components/Socket';
import { useCallback, useEffect, useState } from 'react';
import { MessagesContext } from './MessagesContext';
import {
  saveRoomMessageInLocalStorage,
  savePrivateMessageInLocalStorage,
  getPrivateMessages,
  getRoomMessages
} from './helpers';
import { useAuth } from 'components/Auth';

export const MessagesProvider = ({ children }) => {
  const [messages, setMessages] = useState({});
  const socket = useSocket();
  const { user } = useAuth();

  const loadPrivateMessages = () => {
    const privateMessages = getPrivateMessages(user);

    setMessages((messages) => ({ ...messages, ...privateMessages }));
  };

  const loadRoomMessages = (room) => {
    const roomMessages = getRoomMessages(room);

    setMessages((messages) => ({ ...messages, [room]: roomMessages }));
  };

  // alternative: loadPrivateMessages()
  const addMessage = ({ from, to, own, type, ...restMessage }) => {
    const messagesKey = own || type === 'room' ? to : from;
    const message = {
      ...restMessage,
      to,
      from,
      own
    };

    setMessages((messages) => ({
      ...messages,
      [messagesKey]: messages[messagesKey]
        ? [...messages[messagesKey], message]
        : [message]
    }));
  };

  useEffect(() => user && loadPrivateMessages(), [user]);

  useEffect(() => {
    if (socket) {
      socket.on('new_message', (message) => {
        message.type === 'room' && saveRoomMessageInLocalStorage(message);
        message.type === 'private' && savePrivateMessageInLocalStorage(message);

        addMessage(message);
      });
      socket.on('joined_room', ({ room }) => loadRoomMessages(room));
    }
  }, [socket]);

  const sendMessage = ({ type, ...restMessage }) => {
    const message = {
      ...restMessage,
      from: user
    };

    socket.emit(`${type}_message`, message);
  };

  const getMessages = useCallback((chat) => messages[chat] || [], [messages]);

  const contextValue = {
    getMessages,
    sendMessage
  };

  return (
    <MessagesContext.Provider value={contextValue}>
      {children}
    </MessagesContext.Provider>
  );
};
