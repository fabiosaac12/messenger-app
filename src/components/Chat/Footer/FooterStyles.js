import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  paper: {
    display: 'flex',
    padding: theme.spacing(2),
    borderRadius: '10px',
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
    [theme.breakpoints.down('sm')]: {
      height: '9%',
      borderRadius: 0
    }
  },
  form: {
    width: '100%',
    display: 'flex'
  },
  input: {
    width: '90%'
  },
  button: {
    width: '10%',
    borderRadius: '50px'
  }
}));
