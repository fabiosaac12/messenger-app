import * as Yup from 'yup';
import {
  Paper,
  InputBase,
  ButtonBase,
  Button,
  useTheme
} from '@material-ui/core';
import { Send } from '@material-ui/icons';
import { useIntl } from 'react-intl';
import { useFormik } from 'formik';
import { useMessages } from 'components/Messages';
import { useSelectedChat } from 'components/SelectedChat';
import { useStyles } from './FooterStyles';
import { messages } from './FooterMessages';
import { useModal } from 'components/Modal';
import { useLoader } from 'components/Loader';
import LanguageDetect from 'languagedetect';

const languageDetector = new LanguageDetect();

export const Footer = () => {
  const {
    selectedChat: { type, chat }
  } = useSelectedChat();
  const { sendMessage } = useMessages();
  const classes = useStyles();
  const intl = useIntl();
  const theme = useTheme();
  const { handleOpenModal, handleCloseModal } = useModal();
  const { handleShowLoader } = useLoader();

  const { handleChange, handleSubmit, values } = useFormik({
    initialValues: {
      message: ''
    },
    validationSchema: Yup.object({
      message: Yup.string().required()
    }),
    onSubmit: async ({ message }, { resetForm }) => {
      try {
        handleShowLoader(true);

        const detected = languageDetector.detect(message, 10);

        if (
          detected.length &&
          !detected.some((value) => value[0] === 'english')
        ) {
          handleOpenModal({
            title: 'El texto ingresado no esta escrito en inglés',
            body:
              'Por favor, utiliza únicamente el inglés para mantener conversaciones en esta aplicación y mejorar tus habilidades',
            configProps: {
              maxWidth: 'xs'
            },
            actionButtons: (
              <>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleCloseModal}
                >
                  Está bien
                </Button>
              </>
            )
          });

          return;
        }

        const APIBody = {
          model: 'gpt-3.5-turbo-0125',
          messages: [
            {
              role: 'user',
              content: `Puedes actuar como si fueras un profesor de ingles y explicar los errores (si es que los hay) del siguiente texto? No hagas ninguna introduccion o saludo y habla en espanol. Si la oracion no tiene errores graves responde unicamente con "BIEN": ${message}`
            }
          ]
        };

        const response = await (
          await fetch('https://api.openai.com/v1/chat/completions', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization:
                'Bearer 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'
            },
            body: JSON.stringify(APIBody)
          })
        ).json();

        if (
          response.choices[0].message.content.includes('BIEN') ||
          response.choices[0].message.content === 'Bien' ||
          response.choices[0].message.content === 'Bien.' ||
          response.choices[0].message.content === '¡Bien!'
        ) {
          sendMessage({
            type,
            text: message,
            to: chat
          });
          resetForm();
        } else {
          handleOpenModal({
            title: 'Tengo algunas correcciones para tu texto',
            body: response.choices[0].message.content,
            configProps: {
              maxWidth: 'sm'
            },
            actionButtons: (
              <>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleCloseModal}
                >
                  Lo corregire
                </Button>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => {
                    sendMessage({
                      type,
                      text: message,
                      to: chat
                    });
                    resetForm();
                    handleCloseModal();
                  }}
                >
                  Lo enviare de todas formas
                </Button>
              </>
            )
          });
        }
      } catch (error) {
        sendMessage({
          type,
          text: message,
          to: chat
        });
        resetForm();
      } finally {
        handleShowLoader(false);
      }
    }
  });

  return (
    <Paper className={classes.paper}>
      <form className={classes.form} onSubmit={handleSubmit}>
        <InputBase
          inputProps={{ 'aria-label': 'naked' }}
          name="message"
          className={classes.input}
          value={values.message}
          placeholder={intl.formatMessage(messages.messagePlaceholder)}
          onChange={handleChange}
          onPaste={(event) => event.preventDefault()}
          autoComplete="off"
        />
        <ButtonBase className={classes.button} type="submit">
          <Send />
        </ButtonBase>
      </form>
    </Paper>
  );
};
