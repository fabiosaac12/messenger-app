import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  messagePlaceholder: {
    id: 'components.Chat.Footer.input.message.placeholder',
    defaultMessage: 'Escribe un mensaje...'
  }
});
