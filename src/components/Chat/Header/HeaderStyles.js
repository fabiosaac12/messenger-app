import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  paper: {
    display: 'flex',
    borderRadius: '10px',
    borderBottomLeftRadius: '0px',
    borderBottomRightRadius: '0px',
    [theme.breakpoints.down('sm')]: {
      height: '9%',
      borderRadius: 0
    }
  },
  toolbar: {
    [theme.breakpoints.down('sm')]: {
      minHeight: 'unset'
    }
  }
}));
