import {
  Paper,
  Toolbar,
  ListItemAvatar,
  ListItemText,
  Typography,
  Avatar
} from '@material-ui/core';
import { useSelectedChat } from 'components/SelectedChat';
import { useStyles } from './HeaderStyles';

export const Header = () => {
  const {
    selectedChat: { chat }
  } = useSelectedChat();

  const classes = useStyles();

  return (
    <Paper elevation={1} className={classes.paper}>
      <Toolbar className={classes.toolbar}>
        <ListItemAvatar>
          <Avatar>{chat[0]}</Avatar>
        </ListItemAvatar>
        <ListItemText>
          <Typography variant="subtitle2">{chat}</Typography>
        </ListItemText>
      </Toolbar>
    </Paper>
  );
};
