import { Container, Hidden } from '@material-ui/core';
import { Messages } from 'components/Messages';
import { Header } from './Header';
import { Footer } from './Footer';

export const Chat = () => {
  const content = (
    <>
      <Header />
      <Messages />
      <Footer />
    </>
  );

  return (
    <>
      <Hidden mdUp>{content}</Hidden>
      <Hidden smDown>
        <Container maxWidth="sm">{content}</Container>
      </Hidden>
    </>
  );
};
