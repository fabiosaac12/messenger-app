import { Redirect, Link } from 'react-router-dom';
import * as Yup from 'yup';
import { useIntl } from 'react-intl';
import { Container, Box, Paper, Typography, Button } from '@material-ui/core';
import { Form } from 'components/Form';
import { useAuth } from './hooks/useAuth';
import { useLoader } from 'components/Loader';
import { messages } from './AuthMessages';
import { messages as formMessages } from 'components/Form';

export const Login = () => {
  const { status, login } = useAuth();
  const { handleShowLoader } = useLoader();
  const intl = useIntl();

  const propsToForm = {
    initialValues: { username: '', password: '' },
    fields: [
      {
        type: 'text',
        name: 'username',
        label: intl.formatMessage(messages.usernameLabel),
        placeholder: intl.formatMessage(messages.usernamePlaceholder)
      },
      {
        type: 'password',
        name: 'password',
        label: intl.formatMessage(messages.passwordLabel),
        placeholder: intl.formatMessage(messages.passwordPlaceholder)
      }
    ],
    validationRules: Yup.object({
      username: Yup.string()
        .min(4, intl.formatMessage(formMessages.minFourCharactersError))
        .max(20, intl.formatMessage(formMessages.maxTwentyCharactersError))
        .required(intl.formatMessage(formMessages.requiredFieldError)),
      password: Yup.string().required(
        intl.formatMessage(formMessages.requiredFieldError)
      )
    }),
    handleSubmit: (data) => {
      handleShowLoader(true);

      login(data);
    },
    buttonSubmitLabel: intl.formatMessage(messages.submitLabel)
  };

  return (
    <>
      {status !== 'logged_out' && <Redirect to="/" />}

      <Container maxWidth="sm">
        <Paper>
          <Box p={3}>
            <Box mb={2}>
              <Typography variant="h6" color="textSecondary">
                Iniciar sesión
              </Typography>
            </Box>
            <Form {...propsToForm} />
            <Box mt={3}>
              <Button component={Link} to="/register" variant="text">
                ¿No tienes una cuenta?
              </Button>
            </Box>
          </Box>
        </Paper>
      </Container>
    </>
  );
};
