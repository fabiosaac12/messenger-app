import { Redirect, useHistory, Link } from 'react-router-dom';
import * as Yup from 'yup';
import { useIntl } from 'react-intl';
import { Container, Box, Paper, Typography, Button } from '@material-ui/core';
import { Form } from 'components/Form';
import { useAuth } from './hooks/useAuth';
import { useLoader } from 'components/Loader';
import { messages } from './AuthMessages';
import { messages as formMessages } from 'components/Form';

export const Register = () => {
  const { status, register } = useAuth();
  const { handleShowLoader } = useLoader();
  const intl = useIntl();

  const history = useHistory();

  const propsToForm = {
    initialValues: { email: '', username: '', password: '' },
    fields: [
      {
        type: 'text',
        name: 'email',
        label: intl.formatMessage(messages.emailLabel),
        placeholder: intl.formatMessage(messages.emailPlaceholder)
      },
      {
        type: 'text',
        name: 'username',
        label: intl.formatMessage(messages.usernameLabel),
        placeholder: intl.formatMessage(messages.usernamePlaceholder)
      },
      {
        type: 'password',
        name: 'password',
        label: intl.formatMessage(messages.passwordLabel),
        placeholder: intl.formatMessage(messages.passwordPlaceholder)
      }
    ],
    validationRules: Yup.object({
      email: Yup.string()
        .email(intl.formatMessage(formMessages.invalidEmailError))
        .required(intl.formatMessage(formMessages.requiredFieldError)),
      username: Yup.string()
        .min(4, intl.formatMessage(formMessages.minFourCharactersError))
        .max(20, intl.formatMessage(formMessages.maxTwentyCharactersError))
        .required(intl.formatMessage(formMessages.requiredFieldError)),
      password: Yup.string()
        .min(
          7,
          intl
            .formatMessage(formMessages.minNCharactersError)
            .replace('{{n}}', '7')
        )
        .max(
          100,
          intl
            .formatMessage(formMessages.maxNCharactersError)
            .replace('{{n}}', '100')
        )
        .required(intl.formatMessage(formMessages.requiredFieldError))
    }),
    handleSubmit: async (data) => {
      handleShowLoader(true);

      const user = await register(data);

      handleShowLoader(false);

      if (user) {
        history.replace('login');
      }
    },
    buttonSubmitLabel: intl.formatMessage(messages.submitLabel)
  };

  return (
    <>
      {status !== 'logged_out' && <Redirect to="/" />}

      <Container maxWidth="sm">
        <Paper>
          <Box p={3}>
            <Box mb={2}>
              <Typography variant="h6" color="textSecondary">
                Crear una cuenta
              </Typography>
            </Box>
            <Form {...propsToForm} />
            <Box mt={3}>
              <Button component={Link} to="/login" variant="text">
                ¿Ya tienes una cuenta?
              </Button>
            </Box>
          </Box>
        </Paper>
      </Container>
    </>
  );
};
