import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  emailLabel: {
    id: 'components.Auth.input.email.label',
    defaultMessage: 'Correo electrónico'
  },
  emailPlaceholder: {
    id: 'components.Auth.input.email.Placeholder',
    defaultMessage: 'Ingresar correo electrónico'
  },
  usernameLabel: {
    id: 'components.Auth.input.username.label',
    defaultMessage: 'Nombre de usuario'
  },
  usernamePlaceholder: {
    id: 'components.Auth.input.username.Placeholder',
    defaultMessage: 'Ingresar nombre de usuario'
  },
  passwordLabel: {
    id: 'components.Auth.input.password.label',
    defaultMessage: 'Contraseña'
  },
  passwordPlaceholder: {
    id: 'components.Auth.input.password.Placeholder',
    defaultMessage: 'Ingresar contraseña'
  },
  submitLabel: {
    id: 'components.Auth.button.submit.label',
    defaultMessage: 'Entrar'
  }
});
