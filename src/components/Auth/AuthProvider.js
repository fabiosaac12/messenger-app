import { useState, useEffect } from 'react';
import { useSocket } from 'components/Socket';
import { useLoader } from 'components/Loader';
import { AuthContext } from './AuthContext';
import { getItem, setItem } from 'utils/persistentStorage';
import { useApi } from 'hooks';

export const AuthProvider = ({ children }) => {
  const socket = useSocket();
  const [user, _setUser] = useState(null);
  const [status, setStatus] = useState('loading');
  const { handleShowLoader } = useLoader();

  const _register = useApi({
    endpoint: 'auth/register',
    method: 'post'
  });

  const _login = useApi({
    endpoint: 'auth/login',
    method: 'post'
  });

  const _refresh = useApi({
    endpoint: 'auth/refresh',
    method: 'get'
  });

  const setUser = (username) => {
    const users = getItem('users') || {};

    const updatedUsers = {
      ...users,
      [username]: {
        username,
        rooms: [],
        friends: [],
        chats: {},
        notifications: {},
        ...(users[username] || {})
      }
    };

    setItem('users', updatedUsers);

    _setUser(username);
    setStatus('logged_in');

    socket.emit('logued_in');
    handleShowLoader(false);
  };

  useEffect(() => {
    if (socket) {
      socket.on('user_verified', setUser);
      socket.on('user_already_logged_in', () => {
        setItem('token', '');
        setStatus('logged_out');
      });
    }
  }, [socket]);

  const register = async (data) => {
    try {
      const user = await _register({ body: data });

      return user;
    } catch (error) {
      console.log(error);
    }
  };

  const login = async (data) => {
    try {
      const { token, user } = await _login({ body: data });

      setItem('token', token);

      if (socket.disconnected) {
        socket.connect();
      }

      socket.emit('connected', user.username);

      return true;
    } catch (error) {
      console.log(error);
    }
  };

  const refresh = async () => {
    try {
      const { token, user } = await _refresh({});

      setItem('token', token);
      socket.emit('connected', user.username);
    } catch (error) {
      logout();
    }
  };

  useEffect(() => {
    if (socket) {
      if (getItem('token')) {
        refresh();
      } else {
        logout();
      }
    }
  }, [socket]);

  const logout = async () => {
    try {
      setItem('token', '');
      setStatus('logged_out');
      _setUser(null);

      socket.disconnect();
    } catch {}
  };

  const contextValue = {
    status,
    user,
    setUser,
    login,
    logout,
    register
  };

  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  );
};
