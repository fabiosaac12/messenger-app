import { useAuth } from 'components/Auth';
import { useSocket } from 'components/Socket';
import { useCallback, useEffect, useState } from 'react';
import { getItem } from 'utils/persistentStorage';
import { UsersContext } from './UsersContext';

export const UsersProvider = ({ children }) => {
  const socket = useSocket();
  const [connectedUsers, setConnectedUsers] = useState([]);
  const [friends, setFriends] = useState([]);
  const { user } = useAuth();

  const loadFriends = () => {
    const userFriends = getItem('users')[user].friends;

    setFriends(userFriends);
  };

  useEffect(() => {
    if (user) {
      socket.on('updated_users', loadFriends);
      socket.on('new_message', loadFriends);
    }
  }, [user]);

  useEffect(() => socket && socket.on('updated_users', setConnectedUsers), [
    socket
  ]);

  const getDisconnectedFriends = useCallback(
    () => friends.filter((friend) => !connectedUsers.includes(friend)),
    [friends, connectedUsers]
  );

  const getConnectedFriends = useCallback(
    () => friends.filter((friend) => connectedUsers.includes(friend)),
    [friends, connectedUsers]
  );

  const getConnectedStrangers = useCallback(() =>
    connectedUsers.filter(
      (connectedUser) =>
        !friends.includes(connectedUser) && user !== connectedUser,
      [friends, connectedUsers, user]
    )
  );

  const contextValue = {
    getDisconnectedFriends,
    getConnectedFriends,
    getConnectedStrangers,
    loadFriends
  };

  return (
    <UsersContext.Provider value={contextValue}>
      {children}
    </UsersContext.Provider>
  );
};
