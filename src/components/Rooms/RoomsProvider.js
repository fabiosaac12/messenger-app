import { useAuth } from 'components/Auth';
import { useSocket } from 'components/Socket';
import { useCallback, useEffect, useState } from 'react';
import { setItem, getItem } from 'utils/persistentStorage';
import { RoomsContext } from './RoomsContext';

export const RoomsProvider = ({ children }) => {
  const socket = useSocket();
  const [availableRooms, setAvailableRooms] = useState([]);
  const [userRooms, setUserRooms] = useState([]);
  const { user } = useAuth();

  const getRoomsWhereUserIsNot = useCallback(
    () => availableRooms.filter((room) => !userRooms.includes(room)),
    [availableRooms, userRooms]
  );

  const updateRooms = () => setAvailableRooms(getItem('rooms') || []);

  const joinUserJoinedRooms = () => {
    const userJoinedRooms = getItem('users')[user].rooms;

    for (let room of userJoinedRooms) {
      socket.emit('join_room', room);
    }
  };

  useEffect(() => updateRooms(), []);

  useEffect(() => {
    if (socket) {
      socket.on('updated_rooms', updateRooms);
      socket.on('joined_room', ({ rooms }) => setUserRooms(rooms));
      user && joinUserJoinedRooms();
    }
  }, [socket, user]);

  const createRoom = (room) => {
    const previousRooms = getItem('rooms') || [];

    if (!previousRooms.includes(room)) {
      const nextRooms = [...previousRooms, room];

      setItem('rooms', nextRooms);
      socket.emit('updated_rooms');
    }
  };

  const joinRoom = (room) => {
    const users = getItem('users');
    const userData = users[user];
    const userJoinedRooms = userData.rooms;

    const updatedUsers = {
      ...users,
      [user]: {
        ...userData,
        rooms: [...userJoinedRooms, room]
      }
    };

    setItem('users', updatedUsers);

    socket.emit('join_room', room);
  };

  const contextValue = {
    getAvailableRooms: getRoomsWhereUserIsNot,
    userRooms,
    createRoom,
    joinRoom
  };

  return (
    <RoomsContext.Provider value={contextValue}>
      {children}
    </RoomsContext.Provider>
  );
};
