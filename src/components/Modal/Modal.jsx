import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent
} from '@material-ui/core';
import { useModal } from './hooks/useModal';
import { useStyles } from './ModalStyles';

const Modal = () => {
  const classes = useStyles();
  const { modalState, handleCloseModal } = useModal();
  const {
    show,
    title,
    body,
    actionButtons,
    isDismissible,
    configProps = {}
  } = modalState;
  const {
    maxWidth,
    className,
    titleClassName,
    showDividers,
    scroll,
    ...otherProps
  } = configProps;

  return (
    <Dialog
      open={show}
      aria-labelledby="app-modal"
      onClose={handleCloseModal}
      maxWidth={maxWidth}
      disableBackdropClick={isDismissible ? undefined : true}
      disableEscapeKeyDown={isDismissible ? undefined : true}
      scroll={scroll}
      className={classes.dialog}
      {...otherProps}
    >
      {title && <DialogTitle className={titleClassName}>{title}</DialogTitle>}

      <DialogContent dividers={showDividers}>{body}</DialogContent>

      {actionButtons && <DialogActions>{actionButtons}</DialogActions>}
    </Dialog>
  );
};

export { Modal };
