import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  hello: {
    id: 'components.Modal.hello',
    defaultMessage: 'Hello {name}!!'
  }
});
