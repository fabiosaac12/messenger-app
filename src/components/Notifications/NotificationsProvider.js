import { useAuth } from 'components/Auth';
import { useSelectedChat } from 'components/SelectedChat';
import { useSocket } from 'components/Socket';
import { useEffect, useState } from 'react';
import { getItem, setItem } from 'utils/persistentStorage';
import { NotificationsContext } from './NotificationsContext';

export const NotificationsProvider = ({ children }) => {
  const { user } = useAuth();
  const { selectedChat } = useSelectedChat();
  const socket = useSocket();

  const [notifications, setNotifications] = useState({});

  const setNotificationsFromLocalStorage = () =>
    setNotifications(getItem('users')[user].notifications);

  const updateNotificationsInLocalStorage = (newNotifications) => {
    const users = getItem('users');
    const userData = users[user];
    const updatedUsers = {
      ...users,
      [user]: {
        ...userData,
        notifications: newNotifications
      }
    };

    setItem('users', updatedUsers);
  };

  useEffect(() => user && setNotificationsFromLocalStorage(), [user]);

  useEffect(() => {
    const {
      [selectedChat?.chat]: selectedChatNotifications,
      ...restNotifications
    } = notifications;

    if (selectedChatNotifications) {
      setNotifications(restNotifications);
      updateNotificationsInLocalStorage(restNotifications);
    }
  }, [notifications, selectedChat]);

  useEffect(
    () =>
      socket &&
      user &&
      socket.on('new_message', setNotificationsFromLocalStorage),
    [socket, user]
  );

  const contextValue = notifications;

  return (
    <NotificationsContext.Provider value={contextValue}>
      {children}
    </NotificationsContext.Provider>
  );
};
