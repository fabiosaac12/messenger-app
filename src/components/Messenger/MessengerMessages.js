import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  selectAChat: {
    id: 'components.Messenger.selectAChat',
    defaultMessage: 'Selecciona una chat...'
  }
});
