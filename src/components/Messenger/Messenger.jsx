import { Redirect } from 'react-router-dom';
import { CssBaseline, Typography } from '@material-ui/core';
import { useIntl } from 'react-intl';
import { Chat } from 'components/Chat';
import { useAuth } from 'components/Auth/hooks/useAuth';
import { useSelectedChat } from 'components/SelectedChat';
import { useStyles } from './MessengerStyles';
import { messages } from './MessengerMessages';

export const Messenger = () => {
  const { status } = useAuth();
  const { selectedChat } = useSelectedChat();
  const classes = useStyles();
  const intl = useIntl();

  if (status !== 'logged_in') return <Redirect to="/login" />;

  const content = selectedChat ? (
    <Chat />
  ) : (
    <div className={classes.noChat}>
      <Typography variant="h5">
        {intl.formatMessage(messages.selectAChat)}
      </Typography>
    </div>
  );

  return (
    <>
      <CssBaseline />
      <div className={classes.content}>{content}</div>
    </>
  );
};
