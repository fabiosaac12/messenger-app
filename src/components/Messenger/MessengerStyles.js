import { makeStyles } from '@material-ui/core';
import { sideDrawerWidth } from 'components/SideDrawer';

export const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: theme.spacing(3),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${sideDrawerWidth})`,
      marginLeft: sideDrawerWidth
    },
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(0),
      height: '92vh'
    }
  },
  noChat: {
    justifySelf: 'center',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    '& h5': {
      color: theme.palette.text.secondary
    }
  }
}));
