import React from 'react';
import { AppBar, IconButton, Toolbar, Tooltip } from '@material-ui/core';
import LogoutIcon from '@material-ui/icons/ExitToApp';
import { Theme } from '../Theme';
import { ToggleSideDrawerButton } from 'components/SideDrawer';
import { useStyles } from './HeaderStyles';
import { useAuth } from 'components/Auth';

const Header = () => {
  const auth = useAuth();
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <ToggleSideDrawerButton />
          <Theme />
          <Tooltip title={'Cerrar sesión'}>
            <IconButton edge="start" color="inherit" onClick={auth.logout}>
              <LogoutIcon />
            </IconButton>
          </Tooltip>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export { Header };
