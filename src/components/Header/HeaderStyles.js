import { makeStyles } from '@material-ui/core/styles';
import { sideDrawerWidth } from 'components/SideDrawer';

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up('sm')]: {
      paddingLeft: sideDrawerWidth
    },
    [theme.breakpoints.down('sm')]: {
      height: '8vh'
    }
  },
  appBar: {
    [theme.breakpoints.down('sm')]: {
      height: '100%'
    }
  },
  toolbar: {
    [theme.breakpoints.down('sm')]: {
      minHeight: 'unset',
      height: '100%'
    }
  },
  menuIcon: {
    flexGrow: 1
  }
}));

export { useStyles };
