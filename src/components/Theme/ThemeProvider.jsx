import React, { useState, useMemo, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  createMuiTheme,
  ThemeProvider as MUIThemeProvider
} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { skins, availableSkins } from 'providers/theme';
import { ThemeContext } from './ThemeContext';
import { getItem, setItem } from 'utils/persistentStorage';

const ThemeProvider = (props) => {
  const { children } = props;
  const { LIGHT, DARK } = availableSkins;

  const [theme, setTheme] = useState(
    getItem('theme') ||
      (window.matchMedia &&
      window.matchMedia('(prefers-color-scheme: dark)').matches
        ? DARK
        : LIGHT)
  );

  useEffect(() => {
    setItem('theme', theme);
  }, [theme]);

  const contextValue = useMemo(
    () => ({
      theme,
      availableSkins,
      handleChangeTheme: setTheme
    }),
    [theme]
  );

  const materialTheme = useMemo(
    () =>
      createMuiTheme({
        palette: { type: theme },
        ...skins[theme]
      }),
    [theme]
  );

  return (
    <ThemeContext.Provider value={contextValue}>
      <MUIThemeProvider theme={materialTheme}>
        <CssBaseline />
        {children}
      </MUIThemeProvider>
    </ThemeContext.Provider>
  );
};

ThemeProvider.propTypes = {
  defaultTheme: PropTypes.string,
  children: PropTypes.node.isRequired
};

export { ThemeProvider };
