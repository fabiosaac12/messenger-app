import { useState } from 'react';
import { SideDrawerContext } from './SideDrawerContext';

export const SideDrawerProvider = ({ children }) => {
  const [show, setShow] = useState(false);

  const contextValue = {
    showSideDrawer: show,
    toggleSideDrawer: () => setShow(!show),
    hideSideDrawer: () => setShow(false)
  };

  return (
    <SideDrawerContext.Provider value={contextValue}>
      {children}
    </SideDrawerContext.Provider>
  );
};
