import {
  Avatar,
  Grid,
  Badge,
  Divider,
  Drawer,
  Hidden,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListSubheader,
  Toolbar,
  Typography
} from '@material-ui/core';
import { GroupAdd } from '@material-ui/icons';
import { useIntl } from 'react-intl';
import { useAuth } from 'components/Auth';
import { useUsers } from 'components/Users';
import { useModal } from 'components/Modal';
import { useRooms } from 'components/Rooms';
import { useSelectedChat } from 'components/SelectedChat';
import { useSideDrawer } from './hooks/useSideDrawer';
import { useNotifications } from 'components/Notifications';
import { useStyles } from './SideDrawerStyles.js';
import { generateCreateRoomModalConfig } from './createRoomModal';
import { messages } from './SideDrawerMessages';

export const SideDrawer = () => {
  const { user } = useAuth();
  const { showSideDrawer, toggleSideDrawer, hideSideDrawer } = useSideDrawer();
  const { handleOpenModal, handleCloseModal } = useModal();
  const { setSelectedChat } = useSelectedChat();
  const { getAvailableRooms, userRooms, createRoom, joinRoom } = useRooms();
  const {
    getConnectedFriends,
    getDisconnectedFriends,
    getConnectedStrangers
  } = useUsers();
  const notifications = useNotifications();
  const classes = useStyles();
  const intl = useIntl();

  const openCreateRoomModal = () => {
    const config = generateCreateRoomModalConfig(
      intl,
      handleCloseModal,
      createRoom
    );

    hideSideDrawer();
    handleOpenModal(config);
  };

  const generateChatListItem = (onClick) => (chat, i) =>
    chat !== user && (
      <ListItem
        key={`${chat}${i}`}
        button
        onClick={() => {
          onClick(chat);
          hideSideDrawer();
        }}
        className={classes.listItem}
      >
        <ListItemAvatar>
          <Badge
            color="secondary"
            variant="dot"
            badgeContent={Number(chat in notifications)}
          >
            <Avatar>{chat[0]}</Avatar>
          </Badge>
        </ListItemAvatar>
        <ListItemText>
          <Typography variant="subtitle2">{chat}</Typography>
        </ListItemText>
      </ListItem>
    );

  const generateUserList = (users, label) =>
    users.length > 0 && (
      <>
        <ListSubheader className={classes.listSubheader}>{label}</ListSubheader>
        {users.map(
          generateChatListItem((chat) => setSelectedChat('private', chat))
        )}

        <Divider />
      </>
    );

  const content = user && (
    <Toolbar className={classes.toolbar}>
      <List className={classes.list}>
        <ListItem className={classes.listItem}>
          <Grid container spacing={2} justify="flex-end" alignItems="center">
            <Grid item>
              <ListItemText>
                <Typography variant="subtitle1">{user}</Typography>
              </ListItemText>
            </Grid>
            <Grid item>
              <ListItemAvatar>
                <Avatar>{user[0]}</Avatar>
              </ListItemAvatar>
            </Grid>
          </Grid>
        </ListItem>

        <Divider />

        {generateUserList(
          getConnectedFriends(),
          intl.formatMessage(messages.connectedFriends)
        )}
        {generateUserList(
          getDisconnectedFriends(),
          intl.formatMessage(messages.disconnectedFriends)
        )}
        {generateUserList(
          getConnectedStrangers(),
          intl.formatMessage(messages.connectedStrangers)
        )}

        {/* {userRooms.length > 0 && (
          <>
            <ListSubheader className={classes.listSubheader}>
              {intl.formatMessage(messages.yourRooms)}
            </ListSubheader>
            {userRooms.map(
              generateChatListItem((chat) => setSelectedChat('room', chat))
            )}

            <Divider />
          </>
        )} */}

        {/* <ListSubheader className={classes.listSubheader}>
          {intl.formatMessage(messages.availableRooms)}
        </ListSubheader>
        <ListItem
          button
          onClick={openCreateRoomModal}
          className={classes.listItem}
        >
          <ListItemAvatar>
            <IconButton>
              <GroupAdd />
            </IconButton>
          </ListItemAvatar>
          <ListItemText>
            <Typography variant="subtitle2">
              {intl.formatMessage(messages.createRoom)}
            </Typography>
          </ListItemText>
        </ListItem>

        {getAvailableRooms().map(generateChatListItem(joinRoom))} */}
      </List>
    </Toolbar>
  );

  return (
    <nav className={classes.drawer}>
      <Hidden smUp>
        <Drawer
          variant="temporary"
          open={showSideDrawer}
          onClose={toggleSideDrawer}
        >
          {content}
        </Drawer>
      </Hidden>

      <Hidden xsDown>
        <Drawer variant="permanent" open>
          {content}
        </Drawer>
      </Hidden>
    </nav>
  );
};
