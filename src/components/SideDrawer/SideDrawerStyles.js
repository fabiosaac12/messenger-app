import { makeStyles } from '@material-ui/core';

export const sideDrawerWidth = '250px';

export const useStyles = makeStyles((theme) => ({
  logo: {
    height: '50px',
    marginRight: 'auto'
  },
  toolbar: {
    boxSize: 'border-box',
    padding: '0'
  },
  list: {
    maxHeight: '100%'
  },
  listItem: {
    width: sideDrawerWidth,
    borderTopRightRadius: '20px',
    borderBottomRightRadius: '20px'
  },
  listSubheader: {
    background: theme.palette.background.paper
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: sideDrawerWidth
    }
  },
  togglerButton: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center'
  }
}));
