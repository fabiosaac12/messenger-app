import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  connectedFriends: {
    id: 'components.SideDrawer.section.connectedFriends',
    defaultMessage: 'Amigos conectados'
  },
  disconnectedFriends: {
    id: 'components.SideDrawer.section.disconnectedFriends',
    defaultMessage: 'Amigos desconectados'
  },
  connectedStrangers: {
    id: 'components.SideDrawer.section.connectedStrangers',
    defaultMessage: 'Extraños conectados'
  },
  yourRooms: {
    id: 'components.SideDrawer.section.yourRooms',
    defaultMessage: 'Tus salas'
  },
  availableRooms: {
    id: 'components.SideDrawer.section.availableRooms',
    defaultMessage: 'Salas disponibles'
  },
  createRoom: {
    id: 'components.SideDrawer.createRoom',
    defaultMessage: 'Crear una sala'
  }
});
