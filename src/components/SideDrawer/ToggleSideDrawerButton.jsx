import {
  Tooltip,
  Box,
  IconButton,
  Hidden,
  useTheme,
  useMediaQuery
} from '@material-ui/core';
import { Menu } from '@material-ui/icons';

import { useSideDrawer } from './hooks/useSideDrawer';
import { useStyles } from './SideDrawerStyles';
import { Images } from 'assets';

export const ToggleSideDrawerButton = () => {
  const { toggleSideDrawer } = useSideDrawer();
  const classes = useStyles();
  const theme = useTheme();
  const xs = useMediaQuery(theme.breakpoints.only('xs'));

  return (
    <Box className={classes.togglerButton} mx={1}>
      {!xs && <img className={classes.logo} src={Images.logoWhite} alt="" />}

      <Hidden smUp>
        <Tooltip title="Menu">
          <IconButton onClick={toggleSideDrawer} edge="start" color="inherit">
            <Menu />
          </IconButton>
        </Tooltip>
      </Hidden>
    </Box>
  );
};
