import * as Yup from 'yup';
import { Form } from 'components/Form';
import { messages } from './createRoomModalMessages';
import { messages as formMessages } from 'components/Form';

export const generateCreateRoomModalConfig = (intl, closeModal, createRoom) => {
  const title = intl.formatMessage(messages.title);

  const propsToForm = {
    initialValues: {
      roomName: ''
    },
    fields: [
      {
        type: 'text',
        name: 'roomName',
        label: intl.formatMessage(messages.roomNameLabel),
        placeholder: intl.formatMessage(messages.roomNamePlaceholder)
      }
    ],
    validationRules: Yup.object({
      roomName: Yup.string()
        .min(4, intl.formatMessage(formMessages.minFourCharactersError))
        .max(20, intl.formatMessage(formMessages.maxTwentyCharactersError))
        .required(intl.formatMessage(formMessages.requiredFieldError))
    }),
    handleSubmit: ({ roomName }) => {
      createRoom(roomName);
      closeModal();
    },
    buttonSubmitLabel: intl.formatMessage(messages.submitLabel),
    autocomplete: 'off'
  };

  const body = <Form {...propsToForm} />;

  return {
    title,
    body,
    actionButtons: <div />,
    configProps: {
      maxWidth: 'xs'
    }
  };
};
