import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: {
    id: 'components.SideDrawer.createRoomModal.title',
    defaultMessage: 'Crear una sala'
  },
  roomNameLabel: {
    id: 'components.SideDrawer.createRoomModal.input.roomName.label',
    defaultMessage: 'Nombre de la sala'
  },
  roomNamePlaceholder: {
    id: 'components.SideDrawer.createRoomModal.input.roomName.placeholder',
    defaultMessage: 'Ingresar nombre de la sala'
  },
  submitLabel: {
    id: 'components.SideDrawer.createRoomModal.button.submit.label',
    defaultMessage: 'Crear sala'
  }
});
