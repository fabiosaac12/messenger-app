import { useContext } from 'react';
import { SideDrawerContext } from '../SideDrawerContext';

export const useSideDrawer = () => useContext(SideDrawerContext);
