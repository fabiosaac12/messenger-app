import React from 'react';
import { Grid } from '@material-ui/core';
import { Messenger } from 'components/Messenger';
import './Home.css';

function Home() {
  return (
    <Grid className="home">
      <Messenger />
    </Grid>
  );
}

export { Home };
