import _ from 'lodash';
import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import { routes } from './routes';
import { useAuth } from 'components/Auth';

function Navigation() {
  const auth = useAuth();

  if (auth.status === 'loading') {
    return null;
  }

  return (
    <Router>
      <Switch>
        {_.map(routes, (route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            render={(props) => (
              <route.layout>
                <route.component {...props} />
              </route.layout>
            )}
          />
        ))}
      </Switch>
    </Router>
  );
}

export { Navigation };
