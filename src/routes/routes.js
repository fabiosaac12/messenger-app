import { BaseLayout, LoginLayout, FullScreenLayout } from 'layouts';
import { Home } from 'views/Home';
import { Error404 } from 'views/Error404';
import { Login, Register } from 'components/Auth';

export const routes = [
  {
    path: '/',
    layout: BaseLayout,
    component: Home,
    exact: true
  },
  {
    path: '/login',
    layout: LoginLayout,
    component: Login,
    exact: true
  },
  {
    path: '/register',
    layout: LoginLayout,
    component: Register,
    exact: true
  },
  {
    layout: FullScreenLayout,
    component: Error404
  }
];
