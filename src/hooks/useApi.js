import qs from 'query-string';
import axios from 'axios';
import { useLoader } from 'components/Loader';
import { useAuth } from 'components/Auth';
import { createInstance } from 'utils/api';

export const useApi = ({
  endpoint,
  method,
  baseUrl = 'default',
  withLoader = true
}) => {
  const { handleShowLoader } = useLoader();
  const auth = useAuth();

  const instance = createInstance({ baseUrl });

  const handleFetch = async (
    { body: data, urlParams, queryString },
    headers
  ) => {
    const url = `${endpoint}${urlParams ? `/${urlParams}` : ''}`;

    try {
      if (withLoader) handleShowLoader(true);

      const formattedParams = queryString ?? {};

      const response = await instance({
        method,
        url,
        paramsSerializer: {
          serialize: (params) => qs.stringify(params, { arrayFormat: 'none' })
        },
        headers,
        params: formattedParams,
        data
      });

      return response.data;
    } catch (error) {
      if (axios.isAxiosError(error) && error.response) {
        const response = error.response;

        const unauthorized = response.status === 401;
        const badRequest = response.status === 400;

        if (unauthorized) {
          auth?.logout();
        } else if (badRequest) {
          // TODO: Handle the error 400 here
          // For example, show an alert to the user or log the error.
        } else {
          // TODO: Implement alerts or toast´s for any non-auth error responses
          // TODO: Its posible implement useTranslation here for use common messages
        }

        throw { response, error: response.status };
      }

      throw { error: 1, description: error };
    } finally {
      if (withLoader) handleShowLoader(false);
    }
  };

  return handleFetch;
};
