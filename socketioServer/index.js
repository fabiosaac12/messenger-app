const express = require('express');
const http = require('http');
const socketio = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketio(server, {
  cors: {
    origin: '*'
  }
});

let connectedUsers = {};

io.on('connection', (socket) => {
  let user;

  socket.on('connected', (connectedUser) => {
    user = connectedUser;

    if (user in connectedUsers) {
      socket.emit('user_already_logged_in', user);
    } else {
      connectedUsers[user] = socket.id;

      socket.emit('user_verified', user);
    }
  });

  socket.on('logued_in', () =>
    io.emit('updated_users', Object.keys(connectedUsers))
  );

  socket.on('private_message', ({ to, ...restMessage }) => {
    const message = {
      to,
      from: user,
      ...restMessage,
      type: 'private'
    };

    socket.emit('new_message', { ...message, own: true });

    socket.broadcast.to(connectedUsers[to]).emit('new_message', message);
  });

  socket.on('updated_rooms', () => io.emit('updated_rooms'));

  socket.on('join_room', (room) => {
    socket.join(room);

    const [, ...socketRooms] = [...socket.rooms];

    socket.emit('joined_room', { room, rooms: socketRooms });
  });

  socket.on('room_message', ({ to, ...restMessage }) => {
    const message = {
      to,
      from: user,
      ...restMessage,
      type: 'room'
    };

    socket.emit('new_message', { ...message, own: true });

    socket.broadcast.to(to).emit('new_message', message);
  });

  socket.on('disconnect', () => {
    if (socket.id === connectedUsers[user]) {
      try {
        const { [user]: _, ...restConnectedUsers } = connectedUsers;

        connectedUsers = restConnectedUsers;
      } catch {}

      io.emit('updated_users', Object.keys(connectedUsers));
    }
  });
});

server.listen(4000, () => console.log('ws server in port 4000'));
